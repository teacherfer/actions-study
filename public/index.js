var animals = ["abseiling", "dancing", "doing archery", "doing gymnastics", "drinking tea", "eating a sandwich", "gardening", "climbing", "diving", "swimming", "going trampolining", "playing badminton", "listening to music", "playing table tennis", "playing the guitar", "reading magazines", "staying at home", "writing", "doing my homework", "going shopping", "playing football", "using the computer", "watching TV", "helping home", "walking to school", "visiting my grandparents", "looking after my pet", "talking to my friends"];
var length = animals.length;
var currentPosition = 0;
var speech = null;

initializeItem();
showNavigationButtons();
loadListeners();

function initializeItem()
{
    loadImage();
    loadText();
    loadSpeech();
}

function loadImage()
{
    document.querySelector(".image").src="img/" + animals[currentPosition].toLowerCase() +".jpg";
}

function loadText()
{
    document.querySelector(".text").textContent=animals[currentPosition];
}

function loadSpeech()
{
    speech = new SpeechSynthesisUtterance(animals[currentPosition]);
    speech.lang = 'en-UK';
    document.querySelector(".fa-volume-up").addEventListener("click", playSpeech);
}

function showNavigationButtons()
{
    var fas=document.querySelectorAll('.fas');
    fas.forEach(f => f.style.display="block");
}

function loadListeners()
{
    document.querySelector(".fa-angle-right").addEventListener("click", nextElement);
    document.querySelector(".fa-angle-left").addEventListener("click", previousElement);
}

function playSpeech() {
    window.speechSynthesis.speak(speech);
}

function nextElement() {
    if (currentPosition < (length-1)) {
        currentPosition++;
    }
    else {
        currentPosition = 0;
    }
    initializeItem();
}

function previousElement() {
    if (currentPosition > 0)
    {
        currentPosition--;
    }
    else {
        currentPosition = (length-1);
        console.log(currentPosition);
    }
    initializeItem();
}